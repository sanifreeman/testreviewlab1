package geometry;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void testingGetmethods(){
        Circle c1= new Circle(5.0);
        assertEquals(5.0,c1.getRadius(),0.0001);
    }
    @Test
    public void testGetArea(){
        Circle c2= new Circle(9.0);
        assertEquals(254.47,c2.getArea(),0.1);
    }
    
}
