package geometry;

public class Triangle {
    private double height;
    private double base;

    public Triangle (double height, double base) {
        this.height = height;
        this.base = base;
    }

    public double getHeight() {
        return this.height;
    }

    public double getBase() {
        return this.base;
    }

    public double getArea() {
        return this.base * this.height * 0.5;
    }

    public String toString() {
        return "Height: " + this.height + " , Base: " + this.base;
    }
}
