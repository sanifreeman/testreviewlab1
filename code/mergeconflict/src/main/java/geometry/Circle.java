package geometry;

public class Circle {
    private double radius;
    public Circle(double radius){
        this.radius=radius;
    }
    public double getRadius(){
        return this.radius;
    }
    public double getArea(){
        return Math.pow(this.radius,2)*Math.PI;
    }
    public String toString(){
        return "the rasius is "+radius+".";
    }
}
